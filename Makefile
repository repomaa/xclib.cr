ext: ext/target/release/libxclib.a

ext/target/release/libxclib.a: ext/Cargo.toml
	cd ext && cargo build --release
	objcopy --strip-symbol __mulodi4 $@ $@.stripped
	mv $@.stripped $@

ext/Cargo.toml:
	git clone --depth 1 https://gitlab.com/repomaa/xclib ext

.PHONY: ext

require "./libxclib"

# TODO: Write documentation for `Xclib`
module Xclib
  VERSION = "0.1.3"
  alias Selection = LibXClib::Selection

  def load(selection : Selection = Selection::Primary)
    handle_error LibXClib.load(out data, selection)
    String.new(data)
  end

  def store(data : String, selection : Selection = Selection::Primary)
    handle_error LibXClib.store(data, selection)
  end

  private macro handle_error(call)
    status = {{call}}
    if status != LibXClib::STATUS_OK
      raise String.new(LibXClib.error_string(status))
    end
  end

  extend self
end

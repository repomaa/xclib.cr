@[Link("xcb")]
@[Link("xcb-xfixes")]
@[Link("xcb-shape")]
@[Link("xcb-render")]
{% if flag?(:static) %}
@[Link(ldflags: "-lxclib")]
{% else %}
@[Link(ldflags: "-Wl,-Bstatic -lxclib -Wl,-Bdynamic")]
{% end %}
@[Link(ldflags: "-L#{__DIR__}/../ext/target/release")]

lib LibXClib
  enum Selection
    Primary
    Clipboard
  end

  type Status = Int32
  STATUS_OK = 0

  fun load = xclib_load(data : UInt8**, selection : Selection) : Status
  fun store = xclib_store(data : UInt8*, selection : Selection) : Status
  fun error_string = xclib_error_string(status : Status) : UInt8*
end
